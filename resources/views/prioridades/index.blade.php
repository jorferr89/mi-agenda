@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">PRIORIDADES
                  <div class="btn-group">
                    <a href="{{ route('home') }}" class="btn btn-secondary">
                      Volver
                    </a>
                    <!-- Button formulario -->
					<a href="{{ route('prioridades.crear') }}" class="btn btn-primary">
                      Crear
                    </a>
					

                    
                  </div>
                </div>
                
                <div class="card-body">
                	@include ('layouts.mensaje')
                    <table id="tabla" class="table table-bordered table-hover border-dark">
                        <thead class="thead-light">

						    <tr>
						      <th scope="col">Nombre</th>
						      <th scope="col">Opciones</th>
						    </tr>
						 </thead>
					  <tbody>
					  	@foreach($prioridades as $prioridad)
						    <tr>
							    <td>
		                            {{$prioridad->nombre}}
		                        </td>
		                        <td>
		                        	
		                        	<a class="btn btn-primary" href="{{ route('prioridades.editar', $prioridad)}}">
                                  
                                      Editar
                              		</a>
                              		<!-- Button trigger modal -->
									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminar{{$prioridad->id}}">
									  Eliminar
									</button>


									<!-- Modal -->
									<div class="modal fade" id="eliminar{{$prioridad->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									  <div class="modal-dialog" role="document">

									  	<form action="{{route('prioridades.eliminar', $prioridad)}}" method="POST" class="modal-content">
		                                    @csrf
		                                    @method('DELETE')

										    <div class="modal-content">
										      <div class="modal-header">
										        <h5 class="modal-title" id="exampleModalLabel">ELIMINAR PRIORIDAD</h5>
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
										          <span aria-hidden="true">&times;</span>
										        </button>
										      </div>
										      <div class="modal-body">
										        ¿Está seguro de eliminar la Prioridad {{$prioridad->nombre}}?
										      </div>
										      <div class="modal-footer">
										        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
										        <button type="submit" class="btn btn-danger">Eliminar</button>
										      </div>
										    </div>
										</form>
									  </div>
									</div>
		                        </td>
		                    </tr>
		                @endforeach
					  </tbody>
					</table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#tabla').DataTable({
              "aaSorting": [],
              "pageLength" : 5,
              "lengthMenu": [[5, 10], [5, 10]],
              language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",

                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }

                }

            });




        } );
    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection

