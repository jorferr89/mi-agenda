@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">EDITAR PRIORIDAD
                    <a href="{{ route('prioridades') }}" class="btn btn-secondary">
                      Volver
                    </a>
                </div>

                <div class="card-body">
                    <form class="card-body" method="post" action="{{route('prioridades.actualizar', $prioridad)}}">
            
                        {{ csrf_field() }}
                        {{ method_field('put') }}




                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">NOMBRE *</label>

                            <div class="col-md-6">
                                <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{old('nombre', $prioridad->nombre)}}"  autocomplete="nombre" autofocus>

                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    ACTUALIZAR
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
