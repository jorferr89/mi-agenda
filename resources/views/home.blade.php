@extends('layouts.app')

@section('content')
<div class="container">
    
    <div class="row">

        <div class="col-12 col-lg-6">
            <h2>MI-AGENDA</h2>
            <p class="lead text-secondary">Permite crear, visualizar, actualizar y eliminar sus Trabajos, y puede personalizar y las Prioridades. 
            </p>

            

            <a href="{{route('trabajos')}}" class="btn btn-lg btn-block btn-outline-primary">
                MIS TRABAJOS
            </a>

            <a href="{{route('prioridades')}}" class="btn btn-lg btn-block btn-outline-secondary" >
                PRIORIDADES
            </a>
    
        </div>

        <div class="col-12 col-lg-6 py-3">
            <img class="img-fluid mb-4" src="img/home2.svg" style="width: auto; height: 200px;">
        </div>
    </div>
</div>

@endsection
