@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">MIS TRABAJOS
                  <div class="btn-group">
                    <a href="{{ route('home') }}" class="btn btn-secondary">
                      Volver
                    </a>
                    <a href="{{ route('trabajos.crear') }}" class="btn btn-primary">
                      Crear
                    </a>
                    
                  </div>
                </div>

                <div class="card-body">
                  @include ('layouts.mensaje')
                  <!-- Formulario de bùsqueda -->
                  <form class="form-inline bg-light mb-0 py-2 my-2" method="post" action="{{route('trabajos')}}"> 
                      @csrf
                      
                      <div class="col-2 text-right my-auto text-center text-uppercase font-weight-bold">
                        Desde
                      </div>
                      <div class="form-group col-3">
                        <input type="date" name="fecha_desde" id='fecha_desde' class="form-control @error('fecha_desde') is-invalid @enderror" value="{{old('fecha_desde', $fecha_desde)}}">
                                        
                                        
                      </div>

                      <div class="col-2 text-right my-auto text-center text-uppercase font-weight-bold">
                        Hasta
                      </div>
                      <div class="form-group col-3">
                        <input type="date" name="fecha_hasta" id='fecha_hasta' class="form-control @error('fecha_hasta') is-invalid @enderror" value="{{old('fecha_hasta', $fecha_hasta)}}">
                                        
                                        
                      </div>
                      <div class="col-1 mx-auto">
                        <button type="submit" class="btn btn-primary mb-0">Filtrar</button>
                      </div>
                    </form>



                    <table id="tabla" class="table table-bordered table-hover border-dark">
                        <thead class="thead-light">
                        <tr>
                          <th scope="col">NOMBRE</th>
                          <th scope="col">FECHA LÍMITE</th>
                          <th scope="col">OPCIONES</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($mistrabajos as $mt)
                        <tr>
                            <td>
                                {{$mt->nombre}}
                            </td>

                            <td>
                              <span class="d-none">
                                {{$mt->fecha_limite}}
                            </span>

                                <span class="badge badge-info">{{DateTime::createFromFormat('Y-m-d', $mt->fecha_limite)->format('d/m/Y')}}</span>
                                    
                            </td>

                            <td>
                                <!-- Botòn Ver Trabajo y Eliminar Trabajo -->
                              <button type="button" class="btn btn-info" data-toggle="modal" data-target="#ver{{$mt->id}}">
                                Ver
                              </button>

                              <a class="btn btn-primary" href="{{ route('trabajos.editar', $mt)}}">
                                  
                                      Editar
                              </a>


                              <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminar{{$mt->id}}">
                                Eliminar
                              </button>

                              <!-- Modal Ver Trabajo -->

                              <div class="modal fade" id="ver{{$mt->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLabel">VER TRABAJO</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                    <ul>
                                      <li>NOMBRE: {{$mt->nombre}}</li>
                                      <li>DESCRIPCIÓN: @if($mt->descripcion == null) - @else {{$mt->descripcion}}
                                      @endif</li>
                                      <li>FECHA LÍMITE: <span class="badge badge-info">{{DateTime::createFromFormat('Y-m-d', $mt->fecha_limite)->format('d/m/Y')}}</span></li>
                                      <li>PRIORIDAD: {{$mt->prioridad->nombre}}</li>
                                    </ul>
                                       
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                      
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="modal fade" id="eliminar{{$mt->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">

                                  <form action="{{route('trabajos.eliminar', $mt)}}" method="POST" class="modal-content">
                                                    @csrf
                                                    @method('DELETE')

                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">ELIMINAR TRABAJO</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        ¿Está seguro de eliminar el Trabajo {{$mt->nombre}}?
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                        <button type="submit" class="btn btn-danger">Eliminar</button>
                                      </div>
                                    </div>
                                  </form>
                                </div>
                              </div>



                            </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
      $(document).ready(function(){


      $('#fecha_desde').change(function() {

        var fecha_desde = $(this).val();
        $('#fecha_hasta').attr({"min" : fecha_desde});;


        });

    
      $('#fecha_hasta').change(function() {

        var fecha_hasta = $(this).val();
        $('#fecha_desde').attr({"max" : fecha_hasta});;


        });

      });

    </script>


    <script>
        $(document).ready(function() {
            $('#tabla').DataTable({
              "aaSorting": [],
              "pageLength" : 5,
              "lengthMenu": [[5, 10], [5, 10]],
              language: {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",

                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }

                }

            });




        } );
    </script>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection

