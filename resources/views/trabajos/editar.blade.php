@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">EDITAR TRABAJO
                    <a href="{{ route('trabajos') }}" class="btn btn-secondary">
                      Volver
                    </a>
                </div>

                <div class="card-body">
                    <form class="card-body" method="post" action="{{route('trabajos.actualizar', $trabajo)}}">
            
                        {{ csrf_field() }}
                        {{ method_field('put') }}




                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">NOMBRE *</label>

                            <div class="col-md-6">
                                <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{old('nombre', $trabajo->nombre)}}"  autocomplete="nombre" autofocus>

                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="descripcion" class="col-md-4 col-form-label text-md-right">DESCRIPCIÓN</label>

                            <div class="col-md-6">
                                <textarea type="text" name="descripcion" id='descripcion' class="form-control @error('descripcion') is-invalid @enderror" maxlength="255">{{old('descripcion', $trabajo->descripcion)}}</textarea>
                                    @error('descripcion')
                                    <span class="invalid-feedback" role="alert">
                                        {{$message}}
                                    </span>
                                    @enderror

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fecha_limite" class="col-md-4 col-form-label text-md-right">FECHA LÍMITE *</label>

                            <div class="col-md-6">
                                <input id="fecha_limite" type="date" class="form-control @error('fecha_limite') is-invalid @enderror" name="fecha_limite" value="{{old('fecha_limite', $trabajo->fecha_limite)}}"  autocomplete="fecha_limite">

                                @error('fecha_limite')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="prioridad_id" class="col-md-4 col-form-label text-md-right">PRIORIDAD *</label>

                            <div class="col-md-6">
                                <select id="prioridad_id" name="prioridad_id" class="custom-select @error('prioridad_id') is-invalid @enderror" value="{{old('prioridad_id', $trabajo->prioridad_id)}}" required>
                                    <option value="" selected disabled hidden>- Seleccione un valor -</option>
                                            @foreach($prioridades as $prioridad)
                                                @if (old('prioridades', $trabajo->prioridad_id)== $prioridad->id)
                                                    <option value="{{$prioridad->id}}" selected>
                                                        {{$prioridad->nombre}}
                                                    </option>
                                                @else
                                                    <option value="{{$prioridad->id}}">
                                                        {{$prioridad->nombre}}
                                                    </option>
                                                @endif
                                            @endforeach
                                </select>

                                @error('prioridad_id')
                                <span class="invalid-feedback" role="alert">
                                    {{$message}}
                                </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    ACTUALIZAR
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
