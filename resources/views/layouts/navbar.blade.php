<div class="container">
    <a class="navbar-brand text-dark" href="{{ url('/home') }}">
        Mi-Agenda
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Left Side Of Navbar -->
        <ul class="navbar-nav mr-auto">

        </ul>

        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @guest
                <li class="nav-item">
                    <a class="nav-link text-uppercase text-dark" href="{{ route('login') }}">Ingresar</a>
                </li>
                    
                <li class="nav-item">
                    <a class="nav-link text-uppercase text-dark" href="{{ route('register') }}">Registrarme</a>
                </li>
                    
            @else

                <li class="nav-item">
                    <a class="nav-link text-uppercase text-dark" href="{{ route('home') }}">Inicio</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link text-uppercase text-danger" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">Salir</a>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                </form>

            @endguest
        </ul>
    </div>
</div>
