<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //se definen las FK para la tabla trabajos
        Schema::table('trabajos', function (Blueprint $table) {
            // el mismo nombre de la FK en la migracion, y la referencia con la tabla
            $table->foreign('prioridad_id')->references('id')->on('prioridades');
            $table->foreign('user_id')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trabajos', function (Blueprint $table) {
            $table->dropForeign(['prioridad_id', 'user_id']);
        });
    }
}
