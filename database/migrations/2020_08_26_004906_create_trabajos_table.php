<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrabajosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trabajos', function (Blueprint $table) {
            //id autoincremental
            $table->bigIncrements('id');
            //tabla, tipo de dato, nombre_campo
            $table->string('nombre');
            //campo no obligatorio
            $table->string('descripcion')->nullable();
            $table->date('fecha_limite');
            //FK de la tabla prioridades
            $table->unsignedBigInteger('prioridad_id');
            //FK de la tabla users
            $table->unsignedBigInteger('user_id');
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trabajos');
    }
}
