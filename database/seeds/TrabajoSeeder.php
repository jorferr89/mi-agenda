<?php

use Illuminate\Database\Seeder;

class TrabajoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trabajos')->insert([
	        'nombre'=>'Trabajo Inicial',
	        'descripcion'=> 'Descripción de Trabajo Inicial',
	        'fecha_limite'=> '2020-08-27',
	        'prioridad_id'=>2,
	        'user_id'=>1,
        ]);

        DB::table('trabajos')->insert([
	        'nombre'=>'Creación de Migraciones',
	        'descripcion'=> 'Descripción de Creación de Migraciones',
	        'fecha_limite'=> '2020-08-28',
	        'prioridad_id'=>1,
	        'user_id'=>1,
        ]);

        DB::table('trabajos')->insert([
	        'nombre'=>'Creación de Modelos',
	        'descripcion'=> 'Descripción de Creación de Modelos',
	        'fecha_limite'=> '2020-08-28',
	        'prioridad_id'=>1,
	        'user_id'=>1,
        ]);
    }
}
