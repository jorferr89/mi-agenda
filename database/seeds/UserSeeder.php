<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	//registros
        DB::table('users')->insert([
	        'name'=>'Jorge',
	        'email'=> 'jorge@mail.com',
	        'password'=> bcrypt(12345678),
        ]);
    }
}
