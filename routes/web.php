<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function()
{


	Route::prefix('trabajos')->group(function() {
		//index con la lista de trabajos, la ruta es get y post (x la bùsqueda x fecha)
		Route::match(['get', 'post'], '/', 'TrabajoController@index')->name('trabajos');
		//devuelve el formulario para crear un trabajo
		Route::get('crear', 'TrabajoController@crear')->name('trabajos.crear');
		//ruta post para guardar el trabajo creado
		Route::post('crear', 'TrabajoController@guardar')->name('trabajos.guardar');
		//devuelve el formulario para editar un trabajo
		Route::get('editar/{trabajo}', 'TrabajoController@editar')->name('trabajos.editar');
		//ruta post para actualizar el trabajo creado
		Route::put('editar/{trabajo}', 'TrabajoController@actualizar')->name('trabajos.actualizar');
		//ruta para eliminar un trabajo
		Route::delete('eliminar/{trabajo}', 'TrabajoController@eliminar')->name('trabajos.eliminar');
	});

	Route::prefix('prioridades')->group(function() {
		//ruta index con el listado de prioridades
		Route::get('/', 'PrioridadController@index')->name('prioridades');
		Route::get('crear', 'PrioridadController@crear')->name('prioridades.crear');
		Route::post('crear', 'PrioridadController@guardar')->name('prioridades.guardar');
		Route::get('editar/{prioridad}', 'PrioridadController@editar')->name('prioridades.editar');
		Route::put('editar/{prioridad}', 'PrioridadController@actualizar')->name('prioridades.actualizar');
		Route::delete('eliminar/{prioridad}', 'PrioridadController@eliminar')->name('prioridades.eliminar');
	});

});
