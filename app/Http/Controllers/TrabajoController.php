<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trabajo;
use App\Prioridad;

class TrabajoController extends Controller
{
    public function index(Request $request) { // muestra todos los trabajos del usuario autenticado ordenados por fecha lìmite
        //formulario de bùsqueda
    	$fecha_desde=$request->fecha_desde;
        $fecha_hasta=$request->fecha_hasta;

        if($fecha_desde!=null && $fecha_hasta!=null){
            $request->validate([
                            'fecha_hasta'=>'after_or_equal:fecha_desde',
                            'fecha_desde'=>'before_or_equal:fecha_hasta'
                           ]); 
        }

        $consulta = Trabajo::select('id','nombre', 'descripcion', 'fecha_limite', 'prioridad_id');

        //en el caso que la fecha desde o fecha hasta sea nula

        if($fecha_desde!=null)
            $consulta=$consulta->where('fecha_limite', '>=', $fecha_desde);

        if($fecha_hasta!=null)
            $consulta=$consulta->where('fecha_limite', '<=', $fecha_hasta);

        $mistrabajos=$consulta->whereUser_id(auth()->id())->orderBy('fecha_limite', 'desc')->get();
        

    	//dd($mistrabajos);
    	//retorno la vista, y en compact le paso la variable a recorrer y las de fechas
    	return view('trabajos.index', compact('mistrabajos', 'fecha_desde', 'fecha_hasta'));
    }

    public function crear() { //retorno la vista con el formulario para crear un trabajo
    	//obtengo las prioridades y las devuelvo a la vista
    	$prioridades=Prioridad::orderBy('nombre', 'asc')->get();
    	return view('trabajos.crear', compact('prioridades'));
    }

    public function guardar(Request $request) {
        //validaciones
        $request->validate([
            'nombre'=>'required|string|max:50',
            'descripcion'=>'nullable|string|max:200',
            'fecha_limite'=>'required|date|after_or_equal:today',
            'prioridad_id'=>'required',
        ]);
        
        Trabajo::create([
            'nombre' =>$request->nombre,
            'descripcion' =>$request->descripcion,
            'fecha_limite' =>$request->fecha_limite,
            'prioridad_id'=>$request->prioridad_id,
            'user_id' =>auth()->id(),
        ]);

        return redirect()->route('trabajos')->withMessage('Trabajo guardado');
    }

    public function editar(Trabajo $trabajo) {
        //en el caso que un usuario escriba en el navegador un trabajo a editar que no haya sido creado x el mismo, se redirecciona al home
        if($trabajo->user->id != auth()->id())
            return redirect()->route('home');
        //obtengo las prioridades y las devuelvo a la vista
        $prioridades=Prioridad::orderBy('nombre', 'asc')->get();
        // se retorna la vista del formulario para editar un Trabajo
        return view('trabajos.editar', compact('trabajo','prioridades'));
    }

    public function actualizar(Request $request, Trabajo $trabajo) {

        //validaciones y mensajes
        $request->validate([
            'nombre'=>'required|string|max:50',
            'descripcion'=>'nullable|string|max:200',
            'fecha_limite'=>'required|date|after_or_equal:today',
            'prioridad_id'=>'required',
        ]);
        // se actualiza el registro 
        
        $trabajo->update([
            'nombre' =>$request->nombre,
            'descripcion' =>$request->descripcion,
            'fecha_limite' =>$request->fecha_limite,
            'prioridad_id'=>$request->prioridad_id,
            'user_id' =>auth()->id(),
        ]); 

        return redirect()->route('trabajos')->withMessage('Trabajo actualizado');
    }



    public function eliminar(Trabajo $trabajo) { // elimina un trabajo seleccionado      
        // borra el trabajo y devuelve un mensaje
        $trabajo->delete();
        return redirect()->route('trabajos')->withMessage('Trabajo eliminado');
    }



}
