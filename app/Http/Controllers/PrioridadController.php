<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Trabajo;
use App\Prioridad;

class PrioridadController extends Controller
{
    public function index() { // muestra todas las prioridades ordenados por nombre
    	$prioridades=Prioridad::orderBy('nombre', 'asc')->get();
    	//dd($prioridades);
    	//retorno la vista, y en compact le paso la variable a recorrer
    	return view('prioridades.index', compact('prioridades'));
    }

    public function crear() { //retorno la vista con el formulario para crear una prioridad
        return view('prioridades.crear');
    }

    public function guardar(Request $request) { // guarda una prioridad con los datos ingresados en el formulario

    	//validaciones y mensajes
    	$request->validate([
            'nombre'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:50',
        ]);


    	// cuando pasa las validaciones, se crea una nueva prioridad
        Prioridad::create([
                    'nombre'=>$request->nombre,
                ]);

        //redirecciona a la ruta index de prioridades, y devuelve un mensaje
        return redirect()->route('prioridades')->withMessage('Prioridad guardada');
    }


    public function editar(Prioridad $prioridad) {
        // se retorna la vista del formulario para editar una Prioridad
        return view('prioridades.editar', compact('prioridad'));
    }

    public function actualizar(Request $request, Prioridad $prioridad)
    {
    	// validaciones y mensajes
        $request->validate([
            'nombre'=>'required|regex:/^[a-zA-Z][a-zA-Z ]+$/u|max:50',//regex con espacios
        ]);

        // si pasa las validaciones, se actualiza la prioridad con los nuevos valores ingresados en el formulario por request
        $prioridad->update([
            'nombre' =>$request->nombre,
        ]);

        //retorna la vista con un mensaje
        return redirect()->route('prioridades')->withMessage('Prioridad actualizada');
    }

    public function eliminar(Prioridad $prioridad) { // elimina una prioridad seleccionada

    	//busca las referencias que tiene la prioridad seleccionada para borrar con la tabla trabajos
        $references = Trabajo::wherePrioridad_id($prioridad->id)->count();

        // si uno o mas trabajos tienen asignadas la prioridad que se quiere eliminar, se devuelve un mensaje de error y finaliza la ejecuciòn
        if ($references > 0){ 
            return redirect()->route('prioridades')->withErrors('No se puede eliminar la Prioridad porque hay registros asociados');
        }
        
        // borra la prioridad y devuelve un mensaje
        $prioridad->delete();
        return redirect()->route('prioridades')->withMessage('Prioridad eliminada');
    }

}
