<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trabajo extends Model
{
	//campos que se guardan en la tabla de la BD
    protected $fillable = [
        'nombre', 'descripcion', 'fecha_limite', 'prioridad_id', 'user_id',
    ];

    //tabla en la que se guardan los registros
    protected $table = 'trabajos';

    //para que no almacene los campos created y updated
    public $timestamps = false;

    //representaciòn de la relaciòn 1..N, cuando la tabla tiene el id de la otra, se refencia de èsta manera

    public function prioridad() {
        return $this->belongsTo('App\Prioridad');
    } 

    public function user() {
        return $this->belongsTo('App\User');
    }


}
