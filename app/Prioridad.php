<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prioridad extends Model
{
    //campos que se guardan en la tabla de la BD
    protected $fillable = [
        'nombre',
    ];

    //tabla en la que se guardan los registros
    protected $table = 'prioridades';

    //para que no almacene los campos created y updated
    public $timestamps = false;

    //representaciòn de la relaciòn 1..N, cuando la otra tabla tiene el id, se refencia de èsta manera

    public function trabajos(){
        return $this->hasMany('App\Trabajo');
    } 

}
